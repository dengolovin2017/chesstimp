﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ChessFunctions
{
    class Board //Доска
    {
        public string fen { get; set; } //fen
        Piece[,] pieces; //массив фигур на доске
        public Color moveColor { get; private set; } //кто ходит
        public int moveNumber { get; private set; }
        private int a = -1;
        private bool pJump = true;

        public Board(string fen) //конструктор для подачина на доску fen
        {
            this.fen = fen;
            pieces = new Piece[8, 8]; //инициальзация массива
            Init();
        }

        void Init() //инициальзация фигур на доске
        {
            //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
            // 1                                           2 3    4 5 6
            // 1-фигуры; 2-цвет ходящего; 3-рокеровки; 4-битые поля; 5-50 пустыъ ходов без пешек; 6-номер хода
            string[] parts = fen.Split(); //деление на блоки
            if (parts.Length != 6) return; //если неверно блоков
            InitPieces(parts[0]); //иниц. положения фигур
            moveColor = parts[1] == "b" ? Color.black : Color.white; //инициальзация ходящего (устанавливаем цвет, кто ходит)
            moveNumber = int.Parse(parts[5]);
        }

        void InitPieces(string data) //инициальзация положения фигур
        {
            for (int j = 8; j >= 2; j--)
            {
                data = data.Replace(j.ToString(), (j - 1).ToString() + ".");
            }
            data = data.Replace("1", ".");
            string[] lines = data.Split('/');
            for (int y = 7; y >= 0; y--)
            {
                for (int x = 0; x < 8; x++)
                {
                    pieces[x, y] = lines[7 - y][x] == '.' ? Piece.none : (Piece)lines[7 - y][x];
                }
            }
        }

        public Piece GetPieceAt(Cell cell) //Получить фигуру по ячейке
        {
            if (cell.onBoard()) return pieces[cell.x, cell.y];
            return Piece.none;
        }

        void SetPieceAt(Cell cell, Piece piece) //Задать фигуру на ячейку
        {
            if (cell.onBoard())
            {
                pieces[cell.x, cell.y] = piece;
            }
        }

        public Board Move(PieceMoving pm, char ch) //Сделать ход
        {
            Board nextBoard = new Board(fen); //Создаем новую доску с новыми положениями фигур
            nextBoard.SetPieceAt(pm.from, Piece.none);
            nextBoard.SetPieceAt(pm.to, pm.piece);
            switch (ch)
            {
                case 'q':
                    nextBoard.SetPieceAt(new Cell("c8"), Piece.blackRook);
                    nextBoard.SetPieceAt(new Cell("a8"), Piece.none);  
                    break;
                case 'K' :
                    nextBoard.SetPieceAt(new Cell("h1"), Piece.none);
                    nextBoard.SetPieceAt(new Cell("f1"), Piece.whiteRook);
                    break;
                case 'Q':
                    nextBoard.SetPieceAt(new Cell("a1"), Piece.none);
                    nextBoard.SetPieceAt(new Cell("c1"), Piece.whiteRook);
                    break;
                case 'k':
                    nextBoard.SetPieceAt(new Cell("h8"), Piece.none);
                    nextBoard.SetPieceAt(new Cell("f8"), Piece.blackRook);
                    break;    
            }
            if (moveColor == Color.black) nextBoard.moveNumber++; // +1 к номеру хода 
            nextBoard.moveColor = moveColor.FlipColor(); //флипаем цвет, того кто ходит
            nextBoard.GenerateFEN(); //генерация fen по положению на доске
            return nextBoard;
        }

        public Board Move(PieceMoving pm, Cell pawnCoord = new Cell()) //Сделать ход
        {
            if (pawnCoord.y == 0)
            { 
                pawnCoord = Cell.none;
            }
 
            Board nextBoard = new Board(fen); //Создаем новую доску с новыми положениями фигур
            nextBoard.SetPieceAt(pawnCoord, Piece.none);
            nextBoard.SetPieceAt(pm.from, Piece.none); //Удаляем фигуру с доски, которая сделала ход
            nextBoard.SetPieceAt(pm.to, pm.promotionPawn == Piece.none ? pm.piece : pm.promotionPawn); //Задаем координаты ходящей фигуры
            if (moveColor == Color.black) nextBoard.moveNumber++; // +1 к номеру хода 
            nextBoard.moveColor = moveColor.FlipColor(); //флипаем цвет, того кто ходит
            nextBoard.GenerateFEN(); //генерация fen по положению на доске
            return nextBoard;
        }

        public void PrePawnJump(bool p)
        {
            pJump = p;
        }

        void GenerateFEN() //генерация полного fen
        {
            fen = FenPieces() + " " + (moveColor == Color.white ? "w" : "b")
                + " " + GenRoker() + " " + pj() + " 0 " + moveNumber.ToString();
        }

        private string pj()
        {
            if (pJump)
            {
                return "+";
            } else
            {
                return "-";
            }
        }

        
        string GenRoker() //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
        {
            string rok = "";
            string[] arr = FenPieces().Split('/');

            if (Regex.Match(arr[7], "^R3K.+").Success) rok += "Q";
            if (Regex.Match(arr[7], ".+K2R$").Success) rok += "K";
            if (Regex.Match(arr[0], "^r3k.+").Success) rok += "q";
            if (Regex.Match(arr[0], ".+k2r$").Success) rok += "k";
            if (rok.Equals("")) rok = "-";
            return rok;  
        }
        
        string FenPieces() //Генерация fen - положения фигур
        {
            StringBuilder builder = new StringBuilder();

            for (int y = 7; y >= 0; y--)
            {
                for (int x = 0; x < 8; x++)
                {
                    builder.Append(pieces[x, y] == Piece.none ? '1' : (char)pieces[x, y]);
                }
                if (y > 0)
                {
                    builder.Append('/');
                }
            }
            string ones = "11111111";
            for (int i = 8; i >= 2; i--)
            {
                builder.Replace(ones.Substring(0, i), i.ToString());
            }
            return builder.ToString();
        }

        public IEnumerable<PieceOnCell> EnumPieces()
        {
            foreach (Cell cell in Cell.EnumCells())
                if (GetPieceAt(cell).GetColor() == moveColor)
                    yield return new PieceOnCell(GetPieceAt(cell), cell);

        }

        public bool IsCheck()
        {
            Board after = new Board(fen);
            after.moveColor = moveColor.FlipColor();
            return after.CanEatKing();
        }

        bool CanEatKing()
        {
            Cell badKing = FindBadKing();
            Move moves = new Move(this);
            foreach (PieceOnCell pc in EnumPieces())
            {
                PieceMoving pm = new PieceMoving(pc, badKing);
                if (moves.canMove(pm))
                    return true;

            }
            return false;
        }

        private Cell FindBadKing()
        {
            Piece badKing = moveColor == Color.black ? Piece.whiteKing : Piece.blackKing;
            foreach (Cell cell in Cell.EnumCells())
                if (GetPieceAt(cell) == badKing)
                    return cell;
            return Cell.none;
        }

        public bool IsCkeckAfterMove(PieceMoving pm)
        {
            Board after = Move(pm);
            return after.CanEatKing();
        }       

    }
}
