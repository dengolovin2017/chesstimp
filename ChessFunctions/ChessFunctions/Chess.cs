﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFunctions
{
    public class Chess  
    {
        public string fen; //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
        Board board;
        Move moves;
        List<PieceMoving> allMoves;
        public bool oldHD;
        string dontMove;

        public Chess(string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        {   //конструктор для подачи fen
            this.fen = fen;
            board = new Board(fen);
            moves = new Move(board);
            dontMove = "KQkq";
        }

        public string getMoveColor()
        {
            return fen.Split()[1];
        }

        Chess (Board board, bool old = false, string n = "") //конструктор для обновления доски
        {
            this.board = board;
            this.fen = board.fen; //задание fen шахматам от доски
            moves = new Move(board);
            oldHD = old;
            dontMove = n;
        }

        public char CheckPawnOnFinish()
        {
            string[] fens = fen.Split(' ');
            string[] fin = fens[0].Split('/');

            if (fin[0].Contains('P'))
            {
                return 'P';
            } else if (fin[7].Contains('p'))
            {
                return 'p';
            }
            return ' ';
        }

        public Chess Move(string move) //Ход
        {

            PieceMoving pm = new PieceMoving(move); //Создание хода
            if (!moves.canMove(pm)) //Если ход неверный, то вернем начальные позиции
                return this;
            if (board.IsCkeckAfterMove(pm))
                return this;
            Board nextBoard;
            Chess nextChess;

            if (moves.CanPawnMoveProhod() && oldHD)
            {
                if (pm.piece.GetColor() == Color.white)
                {
                    nextBoard = board.Move(pm, new Cell(pm.to.x, pm.to.y - 1));
                    
                }
                else
                {
                    nextBoard = board.Move(pm, new Cell(pm.to.x, pm.to.y + 1));
                }
                oldHD = false;
                nextChess = new Chess(nextBoard, oldHD, dontMove);
                return nextChess;
            } else if (moves.CanPawnMoveProhod())
            {
                return this;
            }
            
            if ((pm.from.y == 1 && pm.to.y == 3 && pm.piece == Piece.whitePawn) ||
                (pm.from.y == 6 && pm.to.y == 4 && pm.piece == Piece.blackPawn))
            {
                oldHD = true;
            } else
            {
                oldHD = false;
            }

            if (pm.piece == Piece.whiteKing && pm.to.x == 6 && dontMove.Contains('K'))
            {
                dontMove = dontMove.Replace('K', ' ');
                dontMove = dontMove.Replace('Q', ' ');
                nextBoard = board.Move(pm, 'K');
                nextChess = new Chess(nextBoard, oldHD, dontMove);
                return nextChess;
            }
            else if (pm.piece == Piece.blackKing && pm.to.x == 1 && dontMove.Contains('q'))
            {
                dontMove = dontMove.Replace('q', ' ');
                dontMove = dontMove.Replace('k', ' ');
                nextBoard = board.Move(pm, 'q');
                nextChess = new Chess(nextBoard, oldHD, dontMove);
                return nextChess;
            }
            else if (pm.piece == Piece.blackKing && pm.to.x == 6 && dontMove.Contains('k'))
            {
                dontMove = dontMove.Replace('k', ' ');
                dontMove = dontMove.Replace('q', ' ');
                nextBoard = board.Move(pm, 'k');
                nextChess = new Chess(nextBoard, oldHD, dontMove);
                return nextChess;
            }
            else if (pm.piece == Piece.whiteKing && pm.to.x == 1 && dontMove.Contains('Q'))
            {
                dontMove = dontMove.Replace('Q', ' ');
                dontMove = dontMove.Replace('K', ' ');
                nextBoard = board.Move(pm, 'Q');
                nextChess = new Chess(nextBoard, oldHD, dontMove);
                return nextChess;
            }
            
            try
            {
                if (pm.piece == Piece.whiteRook && pm.from.x == 7 && pm.from.y == 0)
                {
                    dontMove = dontMove.Replace('K', ' ');
                } else if (pm.piece == Piece.whiteRook && pm.from.x == 0 && pm.from.y == 0)
                {
                dontMove = dontMove.Replace('Q', ' ');
                }
                else if (pm.piece == Piece.blackRook && pm.from.x == 0 && pm.from.y == 7)
                {
                    dontMove = dontMove.Replace('q', ' ');
                }
                else if (pm.piece == Piece.blackRook && pm.from.x == 7 && pm.from.y == 7)
                {
                    dontMove = dontMove.Replace('k', ' ');
                }
            } catch (Exception ex) { }

            if (pm.piece == Piece.blackKing || pm.piece == Piece.whiteKing)
            {
                if (pm.AbsDeltaX == 2)
                {
                    return this;
                }
            }



            nextBoard = board.Move(pm); //Создание новой доски на основе хода
            nextChess = new Chess(nextBoard, oldHD, dontMove); //Создание новой партии (снапшот) на основе созданной доски
            return nextChess; 
        }

        public char GetPieceAt (int x, int y) //Получение Фигуры по координатам не зная фигуры
        {
            Cell cell = new Cell(x, y); 
            Piece p = board.GetPieceAt(cell); 
            return p == Piece.none ? '.' : (char) p;
        }

        public char GetPieceAt(string position) //Получение Фигуры по координатам не зная фигуры
        {
            Cell cell = new Cell(position);
            Piece p = board.GetPieceAt(cell);
            return p == Piece.none ? '.' : (char)p;
        }

        public void FindAllMoves() //поиск всех ходов
        {
            allMoves = new List<PieceMoving>();
            foreach (PieceOnCell pc in board.EnumPieces())
                foreach (Cell to in Cell.EnumCells())
                {
                    PieceMoving pm = new PieceMoving(pc, to);
                    if (moves.canMove(pm))
                        if (!board.IsCkeckAfterMove(pm))
                            allMoves.Add(pm);
                }
        }

        public List<string> GetAllMoves()
        {
            FindAllMoves();
            List<string> list = new List<string>();

            foreach (PieceMoving pm in allMoves)
                list.Add(pm.ToString());
            return list;
        }

        public bool IsCheck()
        {
            return board.IsCheck();
        }

        public bool IsCheckMAT()
        {
            return GetAllMoves().Count == 0 && board.IsCheck();
        }

        public bool IsCheckPAT()
        {
            return GetAllMoves().Count == 0 && !board.IsCheck();
        }

    }
}
