﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFunctions
{
    class Move //Класс ходы
    {
        PieceMoving pm;
        Board board;


        public Move(Board board) //конструктор создания хода
        {
            this.board = board;
        }

        public bool canMove(PieceMoving pm) //метод проверки возможности хода
        {
            this.pm = pm;
            return CanMoveFrom() && //проверка начального положения
                   CanMoveTo() && //проверка конечного положения
                   CanPieceMove(); //проверка правильности хожа фигуры
        }

        bool CanMoveFrom()
        { 
            return pm.from.onBoard() &&
              board.GetPieceAt(new Cell(pm.from.x, pm.from.y)) == pm.piece &&
              pm.piece.GetColor() == board.moveColor;

        }

        bool CanMoveTo()
        {
            return pm.to.onBoard() &&
                   board.GetPieceAt(pm.to).GetColor() != board.moveColor &&
                   pm.to != pm.from;
        }

        bool CanPieceMove()
        {
            switch (pm.piece)
            {
                case Piece.whiteKing:
                case Piece.blackKing:
                    return CanKingMove() || CanRoker1() || CanRoker2() || CanRoker3() || CanRoker4();
                case Piece.whiteQueen:
                case Piece.blackQueen:
                    return CanStraightMove();
                case Piece.whiteRook:
                case Piece.blackRook:
                    return (pm.SignX == 0 || pm.SignY == 0) &&
                        CanStraightMove();
                case Piece.whiteBishop:
                case Piece.blackBishop:
                    return (pm.SignX != 0 && pm.SignY != 0) &&
                        CanStraightMove();
                case Piece.whiteKnight:
                case Piece.blackKnight:
                    return CanKnightMove();
                case Piece.whitePawn:
                case Piece.blackPawn:
                    return CanPawnMove();

                default: return false;
            }
        }

        

        bool CanPawnMove()
        {
            if (pm.from.y < 1 || pm.from.y > 6)
                return false;
            int stepY = pm.piece.GetColor() == Color.white ? 1 : -1;
            return CanPawnGo(stepY)
                || CanPawnJump(stepY)
                || CanPawnEat(stepY)
                || CanPawnTakingOnThePass(stepY);
        }


        bool CanPawnGo(int stepY)
        {
            if (board.GetPieceAt(pm.to) == Piece.none)
                if (pm.AbsDeltaX == 0)
                    if (pm.DeltaY == stepY)
                    {
                        return true;
                    }
            return false;
        }

        bool CanPawnEat(int stepY)
        {
            if (board.GetPieceAt(pm.to) != Piece.none)
                if (pm.AbsDeltaX == 1)
                    if (pm.DeltaY == stepY)
                    {

                        return true;
                    }
            return false;
        }

        public bool CanPawnMoveProhod()
        {
            if (pm.from.y < 1 || pm.from.y > 6)
                return false;
            int stepY = pm.piece.GetColor() == Color.white ? 1 : -1;
            return CanPawnTakingOnThePass(stepY);
        }

        bool CanPawnTakingOnThePass(int stepY)
        {
            if (board.GetPieceAt(pm.to) == Piece.none)
                if (pm.AbsDeltaX == 1)
                    if (pm.DeltaY == stepY)
                    {
                        if (pm.piece == Piece.blackPawn || pm.piece == Piece.whitePawn)
                        {
                            Cell cell;
                            if (pm.piece.GetColor() == Color.white)
                            {
                                cell = new Cell(pm.to.x, pm.to.y - 1);
                                if (board.GetPieceAt(cell) == Piece.blackPawn)
                                    return true;
                            }
                            else
                            {
                                cell = new Cell(pm.to.x, pm.to.y + 1);
                                if (board.GetPieceAt(cell) == Piece.whitePawn)
                                    return true;
                            }
                        }
                    }
            return false;
                       
        }

        

        bool CanPawnJump(int stepY)
        {
            if (board.GetPieceAt(pm.to) == Piece.none)
                if (pm.AbsDeltaX == 0)
                    if (pm.DeltaY == 2 * stepY)
                        if (pm.from.y == 1 || pm.from.y == 6)
                            if (board.GetPieceAt(new Cell(pm.from.x, pm.from.y + stepY)) == Piece.none)
                            {  
                                return true;
                            }
            return false;
        }

        
        
        
    
        bool CanStraightMove()
        {
            Cell at = pm.from;
            do
            {
                at = new Cell(at.x + pm.SignX, at.y + pm.SignY);
                if (at == pm.to)
                    return true;
            } while (at.onBoard() &&
            board.GetPieceAt(at) == Piece.none);

            return false;
        }

        bool CanKingMove()
        {
            if (pm.AbsDeltaX <= 1 && pm.AbsDeltaY <= 1)
            {
                return true;
            }
            return false;
        }

        bool CanRoker1()
        {
            string rok = board.fen.Split()[2];
            if (rok.Contains('K'))
            {   
                if (pm.to.x == 6 && pm.to.y == 0) return true;
            }
            return false;
        }

        bool CanRoker2()
        {
            string rok = board.fen.Split()[2];
            if (rok.Contains('Q'))
            {
                if (pm.to.x == 1 && pm.to.y == 0) return true;
            }
            return false;
        }

        bool CanRoker3()
        {
            string rok = board.fen.Split()[2];
            if (rok.Contains('k'))
            {
                if (pm.to.x == 6 && pm.to.y == 7) return true;
            }
            return false;
        }
        bool CanRoker4()
        {
            string rok = board.fen.Split()[2];
            if (rok.Contains('q'))
            {
                if (pm.to.x == 1 && pm.to.y == 7) return true;
            }
            return false;
        }

        bool CanKnightMove()
        {
            if (pm.AbsDeltaX == 1 && pm.AbsDeltaY == 2)
            {
               
                return true;
            }
            if (pm.AbsDeltaX == 2 && pm.AbsDeltaY == 1)
            {
                
                return true;
            }
            return false;
        }
        
        

    }
}
