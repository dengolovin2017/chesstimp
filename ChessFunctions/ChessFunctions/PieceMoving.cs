﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFunctions
{
    class PieceMoving //Ход фигуры
    {
        public Piece piece { get; private set; } //Фигура, которая ходит
        public Cell from { get; private set; } //старт ячейка хода
        public Cell to { get; private set; } //финиш ячейка хода
        public Piece promotionPawn { get; private set; } //Если пешка совершает превращение

        public PieceMoving(PieceOnCell pc, Cell to, Piece promotionPawn = Piece.none)
        {   //Конструктор инициальзация 
            this.piece = pc.piece;
            this.from = pc.cell;
            this.to = to;
            this.promotionPawn = promotionPawn;
        }

        public PieceMoving(string move) //Pe2e4 Pe7e8Q
        { //конструктор при вводе хода в виде строки

            this.piece = (Piece)move[0];
            this.from = new Cell(move.Substring(1, 2));
            this.to = new Cell(move.Substring(3, 2));
            this.promotionPawn = (move.Length == 6) ? (Piece)move[5] : Piece.none;
        }

        public int DeltaX { get { return to.x - from.x; } } //количество точек передвижения
        public int DeltaY { get { return to.y - from.y; } }

        public int AbsDeltaX { get { return Math.Abs(DeltaX); } } //модуль
        public int AbsDeltaY { get { return Math.Abs(DeltaY); } }

        public int SignX { get { return Math.Sign(DeltaX); } } //методы узнавания знака для 
        public int SignY { get { return Math.Sign(DeltaY); } } //определения направления движения

       public override string ToString()
        {
            string text = (char)piece + from.Name + to.Name;
            if (promotionPawn != Piece.none)
                text += (char)promotionPawn;
            return text;
        }
    }
}