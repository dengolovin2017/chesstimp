﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessFunctions;
using System.Text.RegularExpressions;

namespace ChessTest //https://www.youtube.com/watch?v=ZuIB4s-5b9g&t=1s  //2.05.30
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
                string a = "r3kbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR";
                string rok = "";
                string[] arr = a.Split('/');
            //Console.WriteLine(arr[7]);
            if (Regex.Match(arr[7], "^R3K.+").Success) rok += "K";
            if (Regex.Match(arr[7], ".+K2R$").Success) rok += "Q";
            if (Regex.Match(arr[0], "^r3k.+").Success) rok += "k";
            if (Regex.Match(arr[0], ".+k2r$").Success) rok += "q";

            Console.WriteLine(rok);
            */
            

            List<string> list;
            Random random = new Random(); 
            Chess chess = new Chess();

            Console.WriteLine(chess.getMoveColor());
            while (true)
            {

                Console.WriteLine(chess.fen);
                Print(ChessToString(chess));
                Console.WriteLine(chess.IsCheck() ? "CHECK" : "");
                foreach (string moves in chess.GetAllMoves())
                    Console.Write(moves + " ");

                list = chess.GetAllMoves();
                if (list.Count == 0 && chess.IsCheckMAT())
                {
                    Console.WriteLine("МАТ");
                    break;
                }
                if (list.Count == 0 && !chess.IsCheckPAT())
                {
                    Console.WriteLine("ПАТ");
                    break;
                }

                Console.WriteLine();
                Console.Write("> ");
                string move = Console.ReadLine();
                if (move == "q") break;
                if (move == "")
                {   
                    move = list[random.Next(list.Count)];
                }
                chess = chess.Move(move);            
            }
        }

        static string ChessToString(Chess chess)
        {
            StringBuilder builder = new StringBuilder("  +-----------------+\n");
            for (int y = 7; y >= 0; y--)
            {
                builder.Append(y + 1);
                builder.Append(" | ");
                for (int x = 0; x < 8; x++)
                {
                    builder.Append(chess.GetPieceAt(x, y));
                    builder.Append(" ");
                }
                builder.Append("|\n");
            }
            builder.Append("  +-----------------+\n");
            builder.Append("    a b c d e f g h\n");           
            return builder.ToString();
        }

        static void Print(string text)
        {
            ConsoleColor oldColor = Console.ForegroundColor;
            foreach (char x in text)
            {
                if (x >= 'a' && x <= 'z')
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                } else if (x >= 'A' && x <= 'Z')
                {
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
                Console.Write(x);
            }
            Console.ForegroundColor = oldColor; 
        } 
    }
}
