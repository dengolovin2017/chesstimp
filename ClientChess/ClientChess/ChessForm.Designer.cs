﻿namespace ClientChess
{
    partial class ChessForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chessBoard = new System.Windows.Forms.Panel();
            this.sideLab = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.главноеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playOnThisDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayOnInternetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MyGamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.JoinGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.sideOnTSP = new System.Windows.Forms.Label();
            this.сдатьсяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer = new System.Windows.Forms.Label();
            this.SH = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chessBoard
            // 
            this.chessBoard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(30)))), ((int)(((byte)(5)))));
            this.chessBoard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chessBoard.Location = new System.Drawing.Point(22, 74);
            this.chessBoard.Name = "chessBoard";
            this.chessBoard.Size = new System.Drawing.Size(675, 675);
            this.chessBoard.TabIndex = 0;
            // 
            // sideLab
            // 
            this.sideLab.AutoSize = true;
            this.sideLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sideLab.Location = new System.Drawing.Point(255, 24);
            this.sideLab.Name = "sideLab";
            this.sideLab.Size = new System.Drawing.Size(226, 46);
            this.sideLab.TabIndex = 1;
            this.sideLab.Text = "Ход Белых";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.главноеToolStripMenuItem,
            this.StatToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(724, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // главноеToolStripMenuItem
            // 
            this.главноеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playOnThisDeviceToolStripMenuItem,
            this.PlayOnInternetToolStripMenuItem,
            this.StateToolStripMenuItem,
            this.сдатьсяToolStripMenuItem});
            this.главноеToolStripMenuItem.Name = "главноеToolStripMenuItem";
            this.главноеToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.главноеToolStripMenuItem.Text = "Главное";
            // 
            // playOnThisDeviceToolStripMenuItem
            // 
            this.playOnThisDeviceToolStripMenuItem.Name = "playOnThisDeviceToolStripMenuItem";
            this.playOnThisDeviceToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.playOnThisDeviceToolStripMenuItem.Text = "играть на одном устройстве";
            this.playOnThisDeviceToolStripMenuItem.Click += new System.EventHandler(this.playOnThisDeviceToolStripMenuItem_Click);
            // 
            // PlayOnInternetToolStripMenuItem
            // 
            this.PlayOnInternetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MyGamesToolStripMenuItem,
            this.JoinGameToolStripMenuItem});
            this.PlayOnInternetToolStripMenuItem.Name = "PlayOnInternetToolStripMenuItem";
            this.PlayOnInternetToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.PlayOnInternetToolStripMenuItem.Text = "играть по интернету";
            // 
            // MyGamesToolStripMenuItem
            // 
            this.MyGamesToolStripMenuItem.Name = "MyGamesToolStripMenuItem";
            this.MyGamesToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.MyGamesToolStripMenuItem.Text = "Своя игра";
            this.MyGamesToolStripMenuItem.Click += new System.EventHandler(this.MyGamesToolStripMenuItem_Click);
            // 
            // JoinGameToolStripMenuItem
            // 
            this.JoinGameToolStripMenuItem.Name = "JoinGameToolStripMenuItem";
            this.JoinGameToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.JoinGameToolStripMenuItem.Text = "Подключится";
            this.JoinGameToolStripMenuItem.Click += new System.EventHandler(this.JoinGameToolStripMenuItem_Click);
            // 
            // StateToolStripMenuItem
            // 
            this.StateToolStripMenuItem.Name = "StateToolStripMenuItem";
            this.StateToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.StateToolStripMenuItem.Text = "статистика";
            this.StateToolStripMenuItem.Click += new System.EventHandler(this.StateToolStripMenuItem_Click);
            // 
            // StatToolStripMenuItem
            // 
            this.StatToolStripMenuItem.Name = "StatToolStripMenuItem";
            this.StatToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.StatToolStripMenuItem.Text = "Статистика";
            this.StatToolStripMenuItem.Click += new System.EventHandler(this.StatToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // sideOnTSP
            // 
            this.sideOnTSP.AutoSize = true;
            this.sideOnTSP.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sideOnTSP.Location = new System.Drawing.Point(532, 43);
            this.sideOnTSP.Name = "sideOnTSP";
            this.sideOnTSP.Size = new System.Drawing.Size(0, 29);
            this.sideOnTSP.TabIndex = 6;
            // 
            // сдатьсяToolStripMenuItem
            // 
            this.сдатьсяToolStripMenuItem.Name = "сдатьсяToolStripMenuItem";
            this.сдатьсяToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.сдатьсяToolStripMenuItem.Text = "сдаться";
            this.сдатьсяToolStripMenuItem.Click += new System.EventHandler(this.lossToolStripMenuItem_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer
            // 
            this.timer.AutoSize = true;
            this.timer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timer.Location = new System.Drawing.Point(41, 43);
            this.timer.Name = "timer";
            this.timer.Size = new System.Drawing.Size(0, 29);
            this.timer.TabIndex = 7;
            // 
            // SH
            // 
            this.SH.AutoSize = true;
            this.SH.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SH.ForeColor = System.Drawing.Color.Red;
            this.SH.Location = new System.Drawing.Point(150, 38);
            this.SH.Name = "SH";
            this.SH.Size = new System.Drawing.Size(0, 29);
            this.SH.TabIndex = 8;
            // 
            // ChessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 761);
            this.Controls.Add(this.SH);
            this.Controls.Add(this.timer);
            this.Controls.Add(this.sideOnTSP);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.sideLab);
            this.Controls.Add(this.chessBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "ChessForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chess";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel chessBoard;
        private System.Windows.Forms.Label sideLab;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem главноеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playOnThisDeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PlayOnInternetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MyGamesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem JoinGameToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label sideOnTSP;
        private System.Windows.Forms.ToolStripMenuItem StatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сдатьсяToolStripMenuItem;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label timer;
        private System.Windows.Forms.Label SH;
    }
}

