﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChessFunctions;

namespace ClientChess
{
    public partial class ChessForm : Form
    {
        private const int SIZE_CELL = 75; //размер одной клетки
        private int TCP_PORT = 13256;
        private TcpListener Listener;
        private TcpClient Client;
        Panel[,] boardPanels; //массив клеток
        Chess chess;
        bool wait;
        bool isTSP;
        int xFrom, yFrom;
        Side side;
        string oldfen;
        List<string> list;
        private DB db;
        private bool inGame;
        int tk, i = 90;


        public ChessForm()
        {
            InitializeComponent();
            InitPanels(); 
            chess = new Chess();
            db = new DB("DataBase1.db"); 
            db.addUser();
        }

        void ArrangeChessPieces() //расставить фигуры //showposition
        {
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    ShowChessPiece(x, y, chess.GetPieceAt(x, y)); //показать
                }
            }
            MarkCellWhere();
        }

        void MarkCellWhere()
        {
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    boardPanels[x, y].BackColor = GetColor(x, y);
                }
            }


            if (wait)
            {
               // MarkCellFrom();
            } else
            {
                MarkCellTo();
            }
        }

        void ShowChessPiece(int x, int y, char piece) //задать значение клетке
        {
            boardPanels[x, y].BackgroundImage = GetPieceImage(piece);
        }

        Image GetPieceImage(char piece) //изьять фигуру из ресурсов
        {
            switch (piece)
            {
                case 'P': return Properties.Resources.WhitePawn;
                case 'R': return Properties.Resources.WhiteRook;
                case 'Q': return Properties.Resources.WhiteQueen;
                case 'K': return Properties.Resources.WhiteKing;
                case 'N': return Properties.Resources.WhiteKnight;
                case 'B': return Properties.Resources.WhiteBishop;

                case 'p': return Properties.Resources.BlackPawn;
                case 'r': return Properties.Resources.BlackRook;
                case 'q': return Properties.Resources.BlackQueen;
                case 'k': return Properties.Resources.BlackKing;
                case 'n': return Properties.Resources.BlackKnight;
                case 'b': return Properties.Resources.BlackBishop;

                default: return null;
            }
        }

        void InitPanels() //инициальзация клеток на доске
        {
            boardPanels = new Panel[8, 8];
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    boardPanels[x, y] = InitChessBoard(x, y);
                }
            }
        }

        public Panel InitChessBoard(int x, int y) //инициализация главной панели
        {
            Panel panel = new System.Windows.Forms.Panel();
            panel.BackColor = GetColor(x, y);
            panel.Location = GetLocation(x, y);
            panel.Name = "c" + x + "" + y;
            panel.Size = new System.Drawing.Size(SIZE_CELL, SIZE_CELL);
            panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            panel.MouseClick += new System.Windows.Forms.MouseEventHandler(panel_MouseClick);
            chessBoard.Controls.Add(panel);
            return panel;
        }

        private Point GetLocation(int x, int y) //получить расположение
        {
            return new Point(SIZE_CELL / 2 + x * SIZE_CELL, SIZE_CELL / 2 + (7 - y) * SIZE_CELL);
        }

        private Color GetColor(int x, int y) //получить цвет клетки по умолчанию
        {
            if ((x + y) % 2 == 0)
            {
                return Color.SaddleBrown;
            } else
            {
                return Color.PeachPuff;     
            }
        }

        private Color GetMarkColor(int x, int y) //пометить клетку
        {
            if ((x + y) % 2 == 0)
            {
                return Color.Coral;
            }
            else
            {
                return Color.Pink;
            }
        }

        private void panel_MouseClick(object sender, MouseEventArgs e) //обработчик нажатия
        {
            CheckMatPAt();

            if (chess.IsCheck())
            {
                SH.Text = "ШАХ";
            } else
            {
                SH.Text = "";
            }
            string oldSide = chess.getMoveColor();

            if (!isTSP || side.ToString().Equals(chess.getMoveColor()))
            {
                string coord = ((Panel)sender).Name.Substring(1);
                int x = coord[0] - '0';
                int y = coord[1] - '0';


    

                if (wait)
                {
                    wait = false;
                    xFrom = x;
                    yFrom = y;
                }
                else
                {
                    oldfen = chess.fen;
                    wait = true;
                    string piece = chess.GetPieceAt(xFrom, yFrom).ToString();
                    string mv = piece + ToCoord(xFrom, yFrom) + ToCoord(x, y);
                    chess = chess.Move(mv);
                    //MessageBox.Show(chess.fen);

                    if (chess.CheckPawnOnFinish() == 'p' || chess.CheckPawnOnFinish() == 'P')
                    {
                        PawnFinishForm pff = new PawnFinishForm();
                        pff.ShowDialog();
                        replacePawn(pff.ChangePiece);

                    }

                    if (isTSP)
                    {
                        if (chess.fen != oldfen)
                        {
                            this.TCP_Send("mov1;" + this.chess.fen);
                        }
                    }

                }
                ArrangeChessPieces();
                checkSide();
                if (chess.getMoveColor() != oldSide)
                    startTimer();
            }
        }

        private void CheckMatPAt()
        {
            list = chess.GetAllMoves();
            if (list.Count == 0 && chess.IsCheckMAT())
            {
                if (isTSP)
                {
                    if (side.ToString() == chess.getMoveColor())
                    {
                        MessageBox.Show("МАТ! Вы проиграли!");
                        db.loss();
                        inGame = false;
                    }
                    else
                    {
                        MessageBox.Show("МАТ! Вы выиграли!");
                        db.win();
                        inGame = false;
                    }
                }
                SH.Text = "МАТ";
                wait = true;
                chess = new Chess();
                checkSide();
                ArrangeChessPieces();
            }
            else if (list.Count == 0 && !chess.IsCheckPAT())
            {
                SH.Text = "ПАТ";
                MessageBox.Show("ПАТ");
                wait = true;
                chess = new Chess();
                checkSide();
                ArrangeChessPieces();
            }
        }

        private void replacePawn(char c)
        {
            string[] old = chess.fen.Split();
            string[] f = chess.fen.Split()[0].Split('/');
            f[0] = f[0].Replace('P', c.ToString().ToUpper()[0]);
            f[7] = f[7].Replace('p', c);
            chess = new Chess(f[0] + "/" + f[1] + "/" + f[2] + "/" + f[3] + "/" + f[4] + "/"
                 + f[5] + "/" + f[6] + "/" + f[7] + " " + old[1] + " " + old[2] + " " + old[3]
                  + " " + old[4] + " " + old[5]);
        }

        private void checkSide()
        {
            if (chess.getMoveColor().Equals("w"))
            {
                sideLab.Text = "Ход Белых";
            }
            else
            {
                sideLab.Text = "Ход Черных";
            }
        }

        /*
        void MarkCellFrom()
        {
            foreach(string move in chess.GetAllMoves())
            {
                int x = move[1] - 'a';
                int y = move[2] - '1';
                //boardPanels[x, y].BackColor = GetMarkColor(x, y);
            }
        }
        */
        void MarkCellTo()
        {
            string pre = chess.GetPieceAt(xFrom, yFrom) + ToCoord(xFrom, yFrom);
            foreach (string move in chess.GetAllMoves())
            {
                
                //boardPanels[2, 2].BackColor = GetMarkColor(2, 2);
                if (move.StartsWith(pre))
                {
                    int x = move[3] - 'a';
                    int y = move[4] - '1';
                    boardPanels[x, y].BackColor = GetMarkColor(x, y);
                }
            }
        }

        private void playOnThisDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Player pla1 = new Player();
            //Player pla2 = new Player();
            /*
            using (var form = new PlaeyrForm("Введите имя игрока", "Введите имя белого игрока"))
            {
                form.ShowDialog();
                MessageBox.Show(form.ReturnName);
            }

            using (var form = new PlaeyrForm("Введите имя игрока", "Введите имя черного игрока"))
            {
                form.ShowDialog();
            }
            */
            timer2.Enabled = true;
            wait = true;
            chess = new Chess();
            checkSide();
            ArrangeChessPieces();
            isTSP = false; 
        }

        private void MyGamesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startTimer();
            inGame = true;

            sideOnTSP.Text = "Вы за белых";

            wait = true;
            chess = new Chess();
            checkSide();
            ArrangeChessPieces();
            isTSP = true;
            side = Side.w;

            this.Listener = new TcpListener(IPAddress.Any, TCP_PORT);
            this.Listener.Start();
            this.Client = this.Listener.AcceptTcpClient();
            this.Listener.Stop();
            timer1.Enabled = true;

        }

        private void JoinGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inGame = true;

            

            using (var form = new PlaeyrForm("Введите адрес сервера", "Введите адрес сервера"))
            {
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    try
                    {
                        this.Client = new TcpClient(form.ReturnName, this.TCP_PORT);
                    }
                    catch (SocketException ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка сокета", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    catch
                    {
                        MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timer1.Enabled = true;
                }
                else
                {
                    return;
                }
            }
            sideOnTSP.Text = "Вы за черных";
            wait = true;
            chess = new Chess();
            checkSide();
            ArrangeChessPieces();
            isTSP = true;
            side = Side.b;
        }

        private void StateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StateForm f = new StateForm();
            f.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //структура комманды
            //xxxx(Команда в 4 символа, 4 означает наличие данных в комманде 1 есть 0 нет);(Разделитель) данные
            //вычитываем поток в цикле
            string Message = "";
            byte[] Buffer = new byte[1];
            int Count;

            try
            {
                while (this.Client.Available > 0)
                {
                    Count = this.Client.GetStream().Read(Buffer, 0, 1);
                    Message += Encoding.ASCII.GetString(Buffer, 0, Count);

                    if (Message.Contains("\r\n\r\n") || Message.Length > 1024)
                    {
                        Message = Message.Replace("\r\n\r\n", "");
                        break;
                    }
                }

                if (Message != "")
                {
                    string Command = string.Empty;
                    if (Message[3] == '0')
                    {
                        Command = Message.Substring(0, 3);
                    }
                    else
                    {
                        Command = Message.Substring(0, 3);
                        string Command_Data = Message.Split(';')[1];

                        switch (Command)
                        {
                            case "mov":
                                if (!side.ToString().Equals(chess.getMoveColor()))
                                {
                                    this.chess = new Chess(Command_Data);
                                    ArrangeChessPieces();
                                    checkSide();
                                }
                                break;
                        }
                    }
                }
            }
            catch (System.IO.IOException e1)
            {
                MessageBox.Show(e1.Message, "Ошибка получения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        private bool TCP_Send(string message)
        {
            try
            {
                message = message + "\r\n\r\n";
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                this.Client.GetStream().Write(data, 0, data.Length);
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message, "Ошибка аргумента", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (System.IO.IOException e)
            {
                MessageBox.Show(e.Message, "Ошибка ввода-вывода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch
            {
                MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void StatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StateForm sf = new StateForm();
            sf.Show();
        }

        private void lossToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //db.loss();
            //inGame = false;
        }

        void startTimer()
        {
            i = 90;
            string c = "1:30";

            timer.Text = c;
            timer2.Interval = 1000;
            timer2.Enabled = true;
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

            tk = --i;
            TimeSpan span = TimeSpan.FromMinutes(tk);
            string label = span.ToString(@"hh\:mm");
            timer.Text = label.ToString();
            if (i < 0)
            {
                timer2.Stop();
                if (chess.getMoveColor() == "w")
                {
                    MessageBox.Show("Белые проиграли");
                    wait = true;
                    chess = new Chess();
                    checkSide();
                    ArrangeChessPieces();
                } else
                {
                    MessageBox.Show("Черные проиграли");
                    wait = true;
                    chess = new Chess();
                    checkSide();
                    ArrangeChessPieces();
                }
                
            }
                

        }

        private string ToCoord(int x, int y) //получить координаты в str
        {
            return ((char)('a' + (char)x)).ToString() + ((char)('1' + (char) y)).ToString();
        }
    }
}
