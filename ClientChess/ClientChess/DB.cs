﻿using System;
using System.Data.SQLite;
using System.IO;

namespace ClientChess
{
    class DB
    {
        private string path;
        private SQLiteConnection con;
        private string folder = "\\dataSQLite";
        private string md = Environment.GetFolderPath(Environment.SpecialFolder.Personal);//путь к Документам

        public DB(string path)
        {

            this.path = path;

            if (Directory.Exists(md + folder) == false)
            {
                Directory.CreateDirectory(md + folder);
            }

            if (!File.Exists(md + folder + "\\" + path))
            {
                SQLiteConnection.CreateFile(path);
                this.con = new SQLiteConnection(@"Data Source=" + (md + folder + "\\" + path) + "; Version=3;");
                SQLiteCommand createtable = new SQLiteCommand("CREATE TABLE IF NOT EXISTS 'players' " +
                    "( 'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 'wins' INTEGER NOT NULL, 'loss' INTEGER NOT NULL);", this.con);
                this.con.Open();
                createtable.ExecuteNonQuery();
                this.con.Close();
            }
            this.con = new SQLiteConnection(@"Data Source=" + (md + folder + "\\" + path) + "; Version=3;");
        }

        public bool addUser()
        {
            
            //SQLiteCommand checker = new SQLiteCommand("SELECT id FROM players WHERE name = '" + name + "'", this.con);
            SQLiteCommand add = new SQLiteCommand("INSERT INTO 'players' ('wins', 'loss') " +
                "VALUES (" + 0 + "," + 0 + ");", this.con);


            SQLiteCommand checker = new SQLiteCommand("SELECT wins, loss FROM players " +
                "WHERE id = 1", this.con);
            
            this.con.Open();
            SQLiteDataReader reader = checker.ExecuteReader();
            if (!reader.HasRows)
            {
                add.ExecuteNonQuery();
            }     
            this.con.Close();
            return true;

        }

        public int getWins()
        {
            SQLiteCommand checker = new SQLiteCommand("SELECT wins FROM players " +
                "WHERE id = 1", this.con);

            this.con.Open();
            //SQLiteDataReader reader = checker.ExecuteReader();
            object a = checker.ExecuteScalar();
            this.con.Close();
            return int.Parse(a.ToString());
        }

        public int getLoss()
        {
            SQLiteCommand checker = new SQLiteCommand("SELECT loss FROM players " +
                "WHERE id = 1", this.con);

            this.con.Open();
            //SQLiteDataReader reader = checker.ExecuteReader();
            object a = checker.ExecuteScalar();
            this.con.Close();
            return int.Parse(a.ToString());
        }

        public void win()
        {
            SQLiteCommand tpd = new SQLiteCommand("UPDATE players SET wins = wins + 1 WHERE id = 1", this.con);
            this.con.Open();
            tpd.ExecuteNonQuery();
            this.con.Close();
        }

        public void loss()
        {
            SQLiteCommand tpd = new SQLiteCommand("UPDATE players SET loss = loss + 1 WHERE id = 1", this.con);
            this.con.Open();
            tpd.ExecuteNonQuery();
            this.con.Close();
        }


    }
}
