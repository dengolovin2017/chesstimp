﻿namespace ClientChess
{
    partial class PawnFinishForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radioFerz = new System.Windows.Forms.RadioButton();
            this.radioRook = new System.Windows.Forms.RadioButton();
            this.radioSlon = new System.Windows.Forms.RadioButton();
            this.radioKnight = new System.Windows.Forms.RadioButton();
            this.Ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выберете фигуру";
            // 
            // radioFerz
            // 
            this.radioFerz.AutoSize = true;
            this.radioFerz.Checked = true;
            this.radioFerz.Location = new System.Drawing.Point(91, 55);
            this.radioFerz.Name = "radioFerz";
            this.radioFerz.Size = new System.Drawing.Size(60, 17);
            this.radioFerz.TabIndex = 1;
            this.radioFerz.TabStop = true;
            this.radioFerz.Text = "Ферзь";
            this.radioFerz.UseVisualStyleBackColor = true;
            // 
            // radioRook
            // 
            this.radioRook.AutoSize = true;
            this.radioRook.Location = new System.Drawing.Point(91, 78);
            this.radioRook.Name = "radioRook";
            this.radioRook.Size = new System.Drawing.Size(57, 17);
            this.radioRook.TabIndex = 2;
            this.radioRook.Text = "Ладья";
            this.radioRook.UseVisualStyleBackColor = true;
            // 
            // radioSlon
            // 
            this.radioSlon.AutoSize = true;
            this.radioSlon.Location = new System.Drawing.Point(91, 101);
            this.radioSlon.Name = "radioSlon";
            this.radioSlon.Size = new System.Drawing.Size(50, 17);
            this.radioSlon.TabIndex = 3;
            this.radioSlon.Text = "Слон";
            this.radioSlon.UseVisualStyleBackColor = true;
            // 
            // radioKnight
            // 
            this.radioKnight.AutoSize = true;
            this.radioKnight.Location = new System.Drawing.Point(91, 124);
            this.radioKnight.Name = "radioKnight";
            this.radioKnight.Size = new System.Drawing.Size(50, 17);
            this.radioKnight.TabIndex = 4;
            this.radioKnight.Text = "Конь";
            this.radioKnight.UseVisualStyleBackColor = true;
            // 
            // Ok
            // 
            this.Ok.Location = new System.Drawing.Point(84, 149);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(75, 23);
            this.Ok.TabIndex = 5;
            this.Ok.Text = "Ок";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // PawnFinishForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 182);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.radioKnight);
            this.Controls.Add(this.radioSlon);
            this.Controls.Add(this.radioRook);
            this.Controls.Add(this.radioFerz);
            this.Controls.Add(this.label1);
            this.Name = "PawnFinishForm";
            this.Text = "PawnFinishForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioFerz;
        private System.Windows.Forms.RadioButton radioRook;
        private System.Windows.Forms.RadioButton radioSlon;
        private System.Windows.Forms.RadioButton radioKnight;
        private System.Windows.Forms.Button Ok;
    }
}