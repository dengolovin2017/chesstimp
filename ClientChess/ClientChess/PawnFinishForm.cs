﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientChess
{
    public partial class PawnFinishForm : Form
    {
        public char ChangePiece { get; set; }

        public PawnFinishForm()
        {
            InitializeComponent();
        }


        private void ChangePawnFinish()
        {
            if (radioFerz.Checked)
            {
                ChangePiece = 'q';
            }
            else if (radioRook.Checked)
            {
                ChangePiece = 'r';
            }
            else if (radioSlon.Checked)
            {
                ChangePiece = 'b';
            }
            else if (radioKnight.Checked)
            {
                ChangePiece = 'n';
            }
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            ChangePawnFinish();
            this.Close();
        }
    }
}
