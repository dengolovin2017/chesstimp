﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientChess
{
    public partial class PlaeyrForm : Form
    {
        public PlaeyrForm(string title, string text)
        {
            InitializeComponent();
            this.Text = title;
            labelPl.Text = text;
        }

        public string ReturnName { get; set; }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.ReturnName = textBoxName.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
