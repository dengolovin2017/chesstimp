﻿namespace ClientChess
{
    partial class StateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxState = new System.Windows.Forms.GroupBox();
            this.Loses = new System.Windows.Forms.Label();
            this.Wins = new System.Windows.Forms.Label();
            this.Amount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxState.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxState
            // 
            this.groupBoxState.Controls.Add(this.Loses);
            this.groupBoxState.Controls.Add(this.Wins);
            this.groupBoxState.Controls.Add(this.Amount);
            this.groupBoxState.Controls.Add(this.label5);
            this.groupBoxState.Controls.Add(this.label3);
            this.groupBoxState.Controls.Add(this.label4);
            this.groupBoxState.Location = new System.Drawing.Point(5, 6);
            this.groupBoxState.Name = "groupBoxState";
            this.groupBoxState.Size = new System.Drawing.Size(184, 106);
            this.groupBoxState.TabIndex = 6;
            this.groupBoxState.TabStop = false;
            // 
            // Loses
            // 
            this.Loses.AutoSize = true;
            this.Loses.Location = new System.Drawing.Point(103, 66);
            this.Loses.Name = "Loses";
            this.Loses.Size = new System.Drawing.Size(75, 13);
            this.Loses.TabIndex = 9;
            this.Loses.Text = "\"Поражений\"";
            // 
            // Wins
            // 
            this.Wins.AutoSize = true;
            this.Wins.Location = new System.Drawing.Point(103, 41);
            this.Wins.Name = "Wins";
            this.Wins.Size = new System.Drawing.Size(49, 13);
            this.Wins.TabIndex = 8;
            this.Wins.Text = "\"Побед\"";
            // 
            // Amount
            // 
            this.Amount.AutoSize = true;
            this.Amount.Location = new System.Drawing.Point(103, 16);
            this.Amount.Name = "Amount";
            this.Amount.Size = new System.Drawing.Size(61, 13);
            this.Amount.TabIndex = 7;
            this.Amount.Text = "\"Сыграно\"";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Поражений";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Побед";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Игр сыграно";
            // 
            // StateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 134);
            this.Controls.Add(this.groupBoxState);
            this.Name = "StateForm";
            this.Text = "StateForm";
            this.groupBoxState.ResumeLayout(false);
            this.groupBoxState.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBoxState;
        private System.Windows.Forms.Label Loses;
        private System.Windows.Forms.Label Wins;
        private System.Windows.Forms.Label Amount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}