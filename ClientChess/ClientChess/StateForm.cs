﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientChess
{
    public partial class StateForm : Form
    {
        private DB db;

        public StateForm()
        {
            InitializeComponent();
            db = new DB("DataBase1.db");
            Amount.Text = (db.getLoss() + db.getWins()).ToString();
            Wins.Text = db.getWins().ToString();
            Loses.Text = db.getLoss().ToString();
        }

    }
}
